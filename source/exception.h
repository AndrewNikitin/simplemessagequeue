#ifndef MMQ_EXCEPTIONS_H
#define MMQ_EXCEPTIONS_H

class EmptyQueueException : public std::exception
{
    virtual const char* what() const noexcept
    {
        return "Got empty queue";
    }
};

class ConnectionTimeoutException : public std::exception
{
    virtual const char* what() const noexcept
    {
        return "Connection timeout";
    }
};

class ConnectionCloseException : public std::exception
{
    virtual const char* what() const noexcept
    {
        return "Connection closed";
    }
};

#endif // MMQ_EXCEPTIONS_H