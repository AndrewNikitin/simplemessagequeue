#ifndef MY_MESSAGE_QUEUE_H
#define MY_MESSAGE_QUEUE_H

#include <iostream>
#include <exception>
#include <queue>
#include <cstdlib>

#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include <string.h>

#include "message.h"
#include "socket.h"
#include "exception.h"

// Functions for debug output
// To enable debug output write #define DEBUG

//#define DEBUG

void DebugInfo()
{
    #ifdef DEBUG
    std::cerr << std::endl;
    #endif
}

template <typename Head, typename... Tail>
void DebugInfo(Head head, Tail... tail)
{
    #ifdef DEBUG
    std::cerr << head << ' ';
    DebugInfo(tail...);
    #endif
}

void* ReadingThread(void *args)
{
    Socket *socket = (Socket*)args;
    Message msg;
    DebugInfo("Start reading from pipe");
    while ( true )
    {
        int rd = read(socket->fd_in, &msg, sizeof(Message));
        if (rd == 0)
        {
            continue;
        }
        if (rd == -1)
        {
            break;
        }
        DebugInfo("Recv message:", msg.size, msg.priority, msg.content);
        if ( msg.priority == -1 )
        {
            socket->closed = true;
            DebugInfo("Got CLOSE_CONNECTION signal");
            break;
        }
        pthread_mutex_lock(&socket->lock_);
        socket->msg_queue.push(msg);
        pthread_mutex_unlock(&socket->lock_);
        // usleep(100);
    }
    DebugInfo("Connection closed");
    return NULL;
}


Socket* Bind(std::string &q_name, int workers=1) 
{
    std::string fifo_b = "/tmp/" + q_name + "_b";
    std::string fifo_c = "/tmp/" + q_name + "_c";
    Socket *socket = new Socket;
    socket->workers = workers;
    socket->closed = false;
    if (mkfifo(fifo_b.data(), 0777) == -1)
    {
        throw std::runtime_error(strerror(errno));
    }
    if ((socket->fd_out = open(fifo_b.data(), O_WRONLY)) == -1)
    {
        throw std::runtime_error(strerror(errno));
    }
    return socket;
}

Socket* Connect(std::string &q_name) 
{
    std::string fifo_b = "/tmp/" + q_name + "_b";
    std::string fifo_c = "/tmp/" + q_name + "_c";
    Socket *socket = new Socket;
    socket->closed = false;
    if ((socket->fd_in = open(fifo_b.data(), O_RDONLY)) == -1)
    {
        throw std::runtime_error(strerror(errno));
    }
    pthread_create(&socket->reading_thread_id, NULL, ReadingThread, (void*)socket);
    pthread_detach(socket->reading_thread_id);
    return socket;
}

void SendMessage(Socket *socket, int size, int priority, const char *buffer)
{
    Message msg(size, buffer, priority);
    DebugInfo("Send message:", msg.size, msg.priority, msg.content);
    write(socket->fd_out, &msg, sizeof(Message));
}

void SendMessage(Socket *socket, Message &msg)
{
    DebugInfo("Send message:", msg.size, msg.priority, msg.content);
    write(socket->fd_out, &msg, sizeof(Message));
} 


void CloseConnection(Socket *socket)
{
    if (socket->type == Socket::SOCTYPE_BINDED)
    {
        for (int i = 0; i < socket->workers; ++i)
        {
            SendMessage(socket, 17, -1, "Close connection");
        }
    }
    close(socket->fd_in);
    close(socket->fd_out);
    free(socket);
}

Message RecvMessage(Socket *socket, bool wait=false) 
{
    Message top_msg;
    bool got_message = false;
    int time = 0;
    do
    {   
        if ( time > Socket::TIMEOUT )
        {
            throw ConnectionTimeoutException();
        }
        pthread_mutex_lock(&socket->lock_);
        if ( !socket->msg_queue.empty() ) 
        {
            top_msg = socket->msg_queue.top();
            socket->msg_queue.pop();
            got_message = true;
        }
        else
        {
            if ( socket->closed )
                throw ConnectionCloseException();
            if ( !wait )
                throw EmptyQueueException();
        }
        pthread_mutex_unlock(&socket->lock_);
        sleep(Socket::RETRY_SECS);
        time += Socket::RETRY_SECS;
   } while(!got_message);
   return top_msg;
}

int RecvMessage(Socket *socket, Message &message, bool wait=false) 
{
    bool got_message = false;
    int wait_time = (wait ? Socket::TIMEOUT : 1);
    for (int time = 0; time < wait_time && !got_message; time += Socket::RETRY_SECS) 
    {
        pthread_mutex_lock(&socket->lock_);
        if (!socket->msg_queue.empty())
        {
            message = socket->msg_queue.top();
            socket->msg_queue.pop();
            got_message = true;
        }
        pthread_mutex_unlock(&socket->lock_);
        if (!got_message)
        {
            if (socket->closed)
                break;
            if (wait)
                sleep(Socket::RETRY_SECS);
        }
    }
    return got_message ? 0 : -1;
}

#endif
