#ifndef MMQ_MESSAGE_H
#define MMQ_MESSAGE_H

class Message
{
public:
    Message(int size, const char* buffer, int priority=0) : size(size), priority(priority)
    {
        memcpy(content, buffer, size);
    }

    Message() : size(0), priority(0) {}

    const static int BUFFER_LEN = 255;

    int size;
    int type;
    int service_code;
    int priority;
    char content[BUFFER_LEN + 1];
};

bool operator<(const Message &lhs,const Message &rhs)
{
    return lhs.priority < rhs.priority;
}

#endif // MMQ_MESSAGE_H 