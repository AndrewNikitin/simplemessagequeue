#ifndef MMQ_SOCKET_H
#define MMQ_SOCKET_H

class Socket
{
public:
    const static int SOCTYPE_BINDED = 0;
    const static int SOCTYPE_CONNECTED = 1;

    const static int TIMEOUT = 15;
    const static int RETRY_SECS = 1;

    int fd_in;
    int fd_out;
    int type;
    int workers;
    bool closed;
    pthread_t reading_thread_id;
    pthread_mutex_t lock_;
    std::string q_name;
    std::priority_queue<Message> msg_queue;
};

#endif // MMQ_SOCKET_H