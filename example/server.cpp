#include <iostream>
#include <string>
#include <ctime>

#include "../source/queue.h"

int main() 
{   
    std::string q_name("my_cool_queue");
    Socket *socket = Bind(q_name, 2);
    int seed = 47; // For test determination
    srand(seed);
    sleep(5); // prepare messages
    for (int i = 0; i < 30; ++i)
    {
        int prior = rand() % 10;
        std::string content = "Message with p=" + std::to_string(prior) 
                               + " i=" + std::to_string(i);
        SendMessage(socket, content.size() + 1, prior, content.data());
    }
    CloseConnection(socket);
}
