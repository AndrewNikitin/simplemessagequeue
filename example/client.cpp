#include <iostream>

#include "../source/queue.h"

void DoSomeJob(const Message &msg)
{
    std::cout << "Priority: " << msg.priority 
              << " Content: \"" << msg.content << "\"" << std::endl;
    sleep(1); // Processing message
}

int main()
{
    std::string q_name("my_cool_queue");
    Socket *socket = Connect(q_name);
    Message msg;
    while ( RecvMessage(socket, msg, true) != -1 )
    {
        DoSomeJob(msg);
    }
    CloseConnection(socket);
}

